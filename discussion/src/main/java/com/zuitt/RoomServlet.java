package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class RoomServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7802515668491130811L;
	
	private ArrayList<String> data = new ArrayList<>();
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" RoomServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		System.getProperties().put("facilities", "Swimmming Pool, Gym, Grand Ballroom, Offices");
		
		String facilities = System.getProperty("facilities");
		
		PrintWriter out = res.getWriter();
		out.println(facilities);
		
		HttpSession session = req.getSession();
		session.setAttribute("availableRooms", "standard");
		
		res.sendRedirect("information?facilities="+facilities);
		
    }
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		String roomName = req.getParameter("roomtype");
		data.add(roomName);
		
		ServletContext srvContext = getServletContext();
		
		srvContext.setAttribute("data", data);
		
		RequestDispatcher rd = req.getRequestDispatcher("information");
		rd.forward(req, res);
		
		PrintWriter out = res.getWriter();
		out.println(data);
    }
	
	
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" RoomServlet has been destroy. ");
		System.out.println("******************************************");
	}

}
